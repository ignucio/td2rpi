.text
.arm
.global _start
_start:
	B inicio

inicio:
	MOV R0, #0x6 @ Inicializo cuanto delay voy a hacer, en un programa real recibiria esto por otra funcion
retardo:
	SUBS R0, R0, #0x1 @ Utilizo el contador descendiente para llevar la cuenta de cuantos
			  @   retardos me faltan, y para hacer el retardo en si
	BNE retardo	  @ Si todavia no lleve el contador a 0, sigo retardando
final:
	B final		  @ Esto es para que si el programa se ejecuta fuera de un debugger, no tire segfault
.end
