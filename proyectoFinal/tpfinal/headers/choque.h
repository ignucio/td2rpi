#ifndef CHOQUE_H
#define CHOQUE_H

int choque(int pines[8], int llaves[4], int pulsadores[2], int periodo, int fdpuerto, bool remoto, bool standalone);

#endif // CHOQUE_H
