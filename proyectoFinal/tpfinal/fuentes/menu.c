#include <ncurses.h>			/* ncurses.h includes stdio.h */  
#include <stdio.h>
#include <wiringPi.h>
#include <string.h> 
#include "../headers/deteccionTecla.h"
#include "../headers/rs232.h"
#include "../headers/auto.h"
#include "../headers/logueo.h"
#include "../headers/carrera.h"
#include "../headers/choque.h"
#include "../headers/apilada.h"
#include "../headers/adcCrudo.h"
#include "../headers/policia.h"
#include "../headers/explosion.h"
#include "../headers/contador.h"
#include "../headers/loteria.h"
#define PUERTO "/dev/serial0"
#define VELOCIDAD 9600
 
int main()
{
	wiringPiSetup();	// Inicializo las funciones de wiringPi, necesario segun documentacion
	initscr();	// Inicializo mi pantalla de ncurses, por defecto llamada stdscr
	noecho(); 		// Deshabilito el echo automatico de los caracteres tipeados
  	keypad(stdscr, TRUE); 	// Deshabilito que se pueda usar el keypad del teclado en la pantalla de ncurses
				//	esto es necesario para el correcto uso de la tecla Escape

	int leds[] ={4, 5, 6, 26, 27, 28, 29, 25};
	int llaves[] ={24, 23, 22, 21};
	int pulsadores[] ={0, 2};
	
	int potenciometro, fotocelula, termistor;

	adcCrudo(1, &potenciometro, &fotocelula, &termistor);

	pinMode(leds[0], OUTPUT);	
	pinMode(leds[1], OUTPUT);
	pinMode(leds[2], OUTPUT);
	pinMode(leds[3], OUTPUT);
	pinMode(leds[4], OUTPUT);
	pinMode(leds[5], OUTPUT);
	pinMode(leds[6], OUTPUT);
	pinMode(leds[7], OUTPUT);

	pinMode(llaves[0], INPUT);
	pinMode(llaves[1], INPUT);
	pinMode(llaves[2], INPUT);
	pinMode(llaves[3], INPUT);

	pinMode(pulsadores[0], INPUT);
	pinMode(pulsadores[1], INPUT);

	bool remoto = 0;
	bool standalone = 0;
	int fdpuerto = 0;
	int filas, columnas;
	int habilitado = 0, rebotado = 0;
	int intentos = 3;
	int posC = 0;
	int velocidad, poteCorrecto;
	int s0=0,s1=0,s2=0,s3=0;

       	potenciometro>5 ? (poteCorrecto = potenciometro - potenciometro%5) : (poteCorrecto = 5);
	velocidad = poteCorrecto;

	initscr();	// Comienzo la interfaz de ncurses

	if(has_colors()){	// Si la terminal soporta colores
 		start_color();	// Arranco ncurses en modo con colores
	}

	init_pair(1, COLOR_WHITE, COLOR_BLACK);	// Defino los colores del par de colores 1
	init_pair(2, COLOR_BLACK, COLOR_WHITE);	// Defino los colores del par de colores 2
	wbkgd(stdscr, COLOR_PAIR(1));	// Seteo el fondo al par de colores 1
	attron(COLOR_PAIR(2));		// Seteo las letras al par de colores 2

	//while(!deteccionTecla('q', 0)){	// Mientras el usuario no aprete Escape
		while(habilitado == 0 && rebotado == 0){	// Mientras el usuario no aprete Escape
			wclear(stdscr);	// Limpio la pantalla al comienzo de cada ciclo
	 		getmaxyx(stdscr, filas, columnas); // Guardo en mis variables las filas 
							   // y columnas de la ventana actual
			habilitado = logueo();
			if(!habilitado) intentos--;
			if(intentos<=0) rebotado = 1;
	 		wrefresh(stdscr); // Actualizo la pantalla en cada ciclo
		}
	//}

	fdpuerto = rs232init(PUERTO,VELOCIDAD);

	while(habilitado && !rebotado && !deteccionTecla('q', 0)){
		wclear(stdscr);	// Limpio la pantalla al comienzo de cada ciclo
 		getmaxyx(stdscr, filas, columnas); // Guardo en mis variables las filas 
						   // y columnas de la ventana actual
 		mvprintw(0	, 0	, "Presione la tecla \"q\" para finalizar", filas, columnas);
 		mvprintw(filas-1, 0	, "[DEBUG] La terminal tiene %d filas y %d columnas", filas, columnas);
		adcCrudo(0, &potenciometro, &fotocelula, &termistor);
       		potenciometro>5 ? (poteCorrecto = potenciometro - potenciometro%5) : (poteCorrecto = 5);
 		mvprintw(filas-2, 0	, "El valor del potenciometro es %d. Para usar este valor como velocidad presione \"p\"", potenciometro);
		(columnas/2-20)<0 ? (posC=0) : (posC = columnas/2-20);
		mvaddstr(filas/2-4, posC, "Ingrese la secuencia que desea comenzar:");
		mvaddstr(filas/2-3, posC, "1) El auto fantástico");
		mvaddstr(filas/2-2, posC, "2) El choque");
		mvaddstr(filas/2-1, posC, "3) La apilada");
		mvaddstr(filas/2+0, posC, "4) La carrera");
		mvaddstr(filas/2+1, posC, "5) El contador");
		mvaddstr(filas/2+2, posC, "6) El auto policia");
		mvaddstr(filas/2+3, posC, "7) La explosion");
		mvaddstr(filas/2+4, posC, "8) La lotería");
		
		int puertochar;
		puertochar = rs232rx(fdpuerto);

		if(standalone){
 			mvprintw(2	, 0	, "Para salir de modo standalone ponga en 0 la llave \"s0\"");
			s0 = digitalRead(llaves[0]);
			while(s0!=0){
				s0 = digitalRead(llaves[0]);
				s1 = digitalRead(llaves[1]);
				s2 = digitalRead(llaves[2]);
				s3 = digitalRead(llaves[3]);
				if (s1==0 && s2==0 && s3==0) velocidad = autoFantastico(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
				if (s1==0 && s2==0 && s3==1) velocidad = choque(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
				if (s1==0 && s2==1 && s3==0) velocidad = apilada(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
				if (s1==0 && s2==1 && s3==1) velocidad = carrera(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
				if (s1==1 && s2==0 && s3==0) velocidad = contador(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
				if (s1==1 && s2==0 && s3==1) velocidad = policia(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
				if (s1==1 && s2==1 && s3==0) velocidad = explosion(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
				if (s1==1 && s2==1 && s3==1) velocidad = loteria(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
			}
			standalone = 0;
		}

		if(!standalone){
 			mvprintw(2	, 0	, "Para pasar a modo standalone ponga el switch \"1\" en high y presione \"x\"");
			if(!remoto){
 				mvprintw(1	, 0	, "Se encuentra en modo LOCAL, para pasar a remoto presione \"r\"");
				if (deteccionTecla('r',1))
					remoto = 1;
			}else{
 				mvprintw(1	, 0	, "Se encuentra en modo REMOTO, para pasar a local presione \"r\"");
 				wrefresh(stdscr); // Actualizo la pantalla en cada ciclo
				rs232escribo(fdpuerto);
				if (deteccionTecla('r',1))
					remoto = 0;
			}

			if (deteccionTecla('x',1) || puertochar=='x') 
				standalone = 1;
			if (deteccionTecla('p',1) || puertochar=='p') 
				velocidad = poteCorrecto;
			if (deteccionTecla('1',1) || puertochar=='1') 
				while(!deteccionTecla('q', 1)) velocidad = autoFantastico(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
			if (deteccionTecla('2',1) || puertochar=='2') 
				while(!deteccionTecla('q', 1)) velocidad = choque(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
			if (deteccionTecla('3',1) || puertochar=='3')
				while(!deteccionTecla('q', 1)) velocidad = apilada(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
			if (deteccionTecla('4',1) || puertochar=='4')
				while(!deteccionTecla('q', 1)) velocidad = carrera(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
			if (deteccionTecla('5',1) || puertochar=='5')
				while(!deteccionTecla('q', 1)) velocidad = contador(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
			if (deteccionTecla('6',1) || puertochar=='6')
				while(!deteccionTecla('q', 1)) velocidad = policia(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
			if (deteccionTecla('7',1) || puertochar=='7')
				while(!deteccionTecla('q', 1)) velocidad = explosion(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
			if (deteccionTecla('8',1) || puertochar=='8')
				while(!deteccionTecla('q', 1)) velocidad = loteria(leds, llaves, pulsadores, velocidad, fdpuerto, remoto, standalone);
 			wrefresh(stdscr); // Actualizo la pantalla en cada ciclo
		}
	}

	rs232close(fdpuerto);
	endwin(); // Cierro la pantalla de ncurses
	return 0;
}
