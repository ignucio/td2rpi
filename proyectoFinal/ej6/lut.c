#include <wiringPi.h>	// -lwiringPi
#include <stdint.h>	// Necesario para usar uint8_t y asegurar portabilidad de tamaño de variable
#include <ncurses.h>	// -lncurses

bool finalizar(void){	// Funcion que detecta presion de tecla, sin retrasar la ejecucion del programa
	int c;	// Caracter a recibir
	bool r; // Valor de retorno, que indica si recibi caracter o no

	nodelay(stdscr, TRUE);	// Desactivo la espera a que se escriba un caracter,
				//   esto es lo que hace que no se pause la ejecucion
	noecho();		// Desactivo que los caracteres tipeados salgan en pantalla

	c = getch();		// Veo si puedo recibir un caracter

	if(c==ERR){		// Si no recibi nada, y se timeouteo la funcion getch, 
				//   porque no tiene delay, tendra error en la variable
		r = 0;		// En cuyo caso, no recibi nada
	}else{
		r = 1;		// Si en 'c' tengo guardado cualquier cosa que no sea error,
	       			//   es que recibi algun caracter
		ungetch(c);	// Lo devuelvo al buffer de caracteres de stdin,
	       			//   por si otro programa necesita usar ese caracter
	}

	echo();			// Vuelvo a habilitar que se muestren caracteres por pantalla
	nodelay(stdscr, FALSE); // Vuelvo a habilitar que la funcion getch espere a que el usuario teclee

	return(r);	// Devuelvo si recibi, o no, un caracter
}


uint8_t carrera(uint8_t pos){	// Funcion que recibe posicion de la look up table y devuelve el valor
	static const uint8_t lut[18] =	// La LUT se implementa con static y const para permitir optimizaciones
	{
		// Valores calculados manualmente, es simplemente pasar del binario de 8 leds al hexa correspondiente
		0x00U, 0x00U,
		0x01U, 0x01U,
		0x02U, 0x02U,
		0x04U, 0x04U,
		0x08U, 0x08U,
		0x11U, 0x12U,
		0x24U, 0x28U,
		0x50U, 0x60U,
		0xC0U, 0x80U,
	};
	return lut[pos];	// Devuelvo el valor de la tabla en la posicion que me pidieron
}

int main(void){
	wiringPiSetup(); 	// Inicializo la libreria de wiringPi, necesario segun documentacion

	initscr();	// Inicializo mi pantalla de ncurses, por defecto llamada stdscr
	mvaddstr(0,0,"Presione cualquier tecla para finalizar"); // Le indico al usuario como cortar secuencia de luces

	// Seteo todos mis leds como salidas
	pinMode(4, OUTPUT);
	pinMode(5, OUTPUT);
	pinMode(6, OUTPUT);
	pinMode(26, OUTPUT);
	pinMode(27, OUTPUT);
	pinMode(28, OUTPUT);
	pinMode(29, OUTPUT);
	pinMode(25, OUTPUT);


	while(!finalizar()){	// Mientras el usuario no decidio finalizar secuencia
		for(int i=0; i<18; i++){	// Recorro los 18 valores de mi LUT
			if(!finalizar()){	// Comparacion para poder salir sin esperar a que termine el ciclo
				(carrera(i) & 0x01) ? digitalWrite(4, HIGH) : digitalWrite(4, LOW);
				(carrera(i) & 0x02) ? digitalWrite(5, HIGH) : digitalWrite(5, LOW);
				(carrera(i) & 0x04) ? digitalWrite(6, HIGH) : digitalWrite(6, LOW);
				(carrera(i) & 0x08) ? digitalWrite(26, HIGH) : digitalWrite(26, LOW);
				(carrera(i) & 0x10) ? digitalWrite(27, HIGH) : digitalWrite(27, LOW);
				(carrera(i) & 0x20) ? digitalWrite(28, HIGH) : digitalWrite(28, LOW);
				(carrera(i) & 0x40) ? digitalWrite(29, HIGH) : digitalWrite(29, LOW);
				(carrera(i) & 0x80) ? digitalWrite(25, HIGH) : digitalWrite(25, LOW);
				delay(200);	// Retardo de la funcion
			} else {
				break;
			}
		}	
	}

	// Pongo todos los leds a 0
	digitalWrite(4, LOW);
	digitalWrite(5, LOW);
	digitalWrite(6, LOW);
	digitalWrite(26, LOW);
	digitalWrite(27, LOW);
	digitalWrite(28, LOW);
	digitalWrite(29, LOW);
	digitalWrite(25, LOW);

	endwin();	// Cierro la pantalla, necesario segun documentacion de ncurses

	return 0;
}
