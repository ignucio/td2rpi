#include <wiringPi.h>	// -lwiringPi
#include <ncurses.h>	// -lncurses

bool finalizar(void){	// Funcion que detecta presion de tecla, sin retrasar la ejecucion del programa
	int c;	// Caracter a recibir
	bool r; // Valor de retorno, que indica si recibi caracter o no

	nodelay(stdscr, TRUE);	// Desactivo la espera a que se escriba un caracter,
				//   esto es lo que hace que no se pause la ejecucion
	noecho();		// Desactivo que los caracteres tipeados salgan en pantalla

	c = getch();		// Veo si puedo recibir un caracter

	if(c==ERR){		// Si no recibi nada, y se timeouteo la funcion getch, 
				//   porque no tiene delay, tendra error en la variable
		r = 0;		// En cuyo caso, no recibi nada
	}else{
		r = 1;		// Si en 'c' tengo guardado cualquier cosa que no sea error,
	       			//   es que recibi algun caracter
		ungetch(c);	// Lo devuelvo al buffer de caracteres de stdin,
	       			//   por si otro programa necesita usar ese caracter
	}

	echo();			// Vuelvo a habilitar que se muestren caracteres por pantalla
	nodelay(stdscr, FALSE); // Vuelvo a habilitar que la funcion getch espere a que el usuario teclee

	return(r);	// Devuelvo si recibi, o no, un caracter
}

int main(void){
	wiringPiSetup();	// Inicializo las funciones de wiringPi, necesario segun documentacion
	
	// Seteo mis leds como salida y mis switches y pulsadores como entrada
	pinMode(4 , OUTPUT);	
	pinMode(5 , OUTPUT);
	pinMode(6 , OUTPUT);
	pinMode(26, OUTPUT);
	pinMode(27, OUTPUT);
	pinMode(28, OUTPUT);
	pinMode(29, OUTPUT);
	pinMode(25, OUTPUT);

	pinMode(0, INPUT);
	pinMode(2, INPUT);

	pinMode(21, INPUT);
	pinMode(22, INPUT);
	pinMode(23, INPUT);
	pinMode(24, INPUT);

	initscr();	// Inicializo mi pantalla de ncurses, por defecto llamada stdscr
	mvaddstr(0,0,"Presione cualquier tecla para finalizar"); // Le indico al usuario como cortar secuencia de luces


	bool valores[8] = {0,0,0,0,0,0,0,0}; // Cadena que guarda estados de los leds

	while(1){
		if(!finalizar()){	// Si el usuario no decidio terminar todavia
			// Primero leo todos los switches, e igualo los valores a lo que corresponda
			valores[0] = digitalRead(21) & digitalRead(0);
			valores[4] = digitalRead(21) & digitalRead(2);
			valores[1] = digitalRead(22) & digitalRead(0);
			valores[5] = digitalRead(22) & digitalRead(2);
			valores[2] = digitalRead(23) & digitalRead(0);
			valores[6] = digitalRead(23) & digitalRead(2);
			valores[3] = digitalRead(24) & digitalRead(0);
			valores[7] = digitalRead(24) & digitalRead(2);
			// Igualo todos los pines a sus respectivos valores
			digitalWrite(4 ,valores[0]);
			digitalWrite(5 ,valores[1]);
			digitalWrite(6 ,valores[2]);
			digitalWrite(26,valores[3]);
			digitalWrite(27,valores[4]);
			digitalWrite(28,valores[5]);
			digitalWrite(29,valores[6]);
			digitalWrite(25,valores[7]);
		}else{
			// Cuando termina el programa apagado todos los pines
			digitalWrite(4,LOW);
			digitalWrite(5,LOW);
			digitalWrite(6,LOW);
			digitalWrite(26,LOW);
			digitalWrite(27,LOW);
			digitalWrite(28,LOW);
			digitalWrite(29,LOW);
			digitalWrite(25,LOW);

			mvaddstr(2,0,"Finalizo la ejecucion, presione cualquier tecla para cerrar el programa.");
			getch(); // El primer getch es para recibir el caracter que triggereo la funcion finalizar
			getch(); // El segundo getch es para esperar otro caracter, 
				 //   para que el usuario pueda cortar la ejecucion
			break;
		}
	}
	endwin();	// Cierro la pantalla, necesario segun documentacion de ncurses
	return 0;	// Retorno 0 por main
}
