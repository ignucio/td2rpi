.text
.global _start
_start:
	MOV R0, #0x1 @ Esta es la variable del case, en un programa real la recibiria de la funcion llamadora
caso1:
	CMP R0, #0x0 @ Estoy en el caso 1?
	BNE caso2 @ Si no es el caso 1, pruebo el caso 2
		@ Aca iria el codigo del caso 1
	B final @ Break
caso2:
	CMP R0, #0x1 @ Estoy en el caso 2?
	BNE caso3 @ Si no es el caso 2, pruebo el caso 3
		@ Aca iria el codigo del caso 2
	B final @ Break
caso3:
	CMP R0, #0x2 @ Estoy en el caso 3?
	BNE caso4 @ Si no es el caso 3, pruebo el caso 4
		@ Aca iria el codigo del caso 3
	B final @ Break
caso4:
	CMP R0, #0x3 @ Estoy en el caso 4?
		@ Aca iria el codigo del caso 4
	B final @ Break
final:
	B final @ Evita que el programa consuma meoria haciendo NOPS y tire segfault
	.end
